#!/usr/bin/env ruby
#encoding: utf-8

def test questions
  questions.each do |q|
    puts "#{q} #{NONN::Main.noui(q).ass}"
  end
end

def test1 questions
  require_relative '../lib/niouininon.rb'
  test(questions)
end

def test2 questions
  require 'niouininon'
  test(questions)
end

questions = []
questions <<  "Bonjour, ca va ?"
questions <<  "J'aime les trains, pas toi ?"
questions <<  "C'est trop mignon, tu m'aimes ?"
questions <<  "Oh t'as combien de doigts ?"

test1(questions)
test2(questions)
