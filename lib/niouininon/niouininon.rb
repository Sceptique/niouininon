#!/usr/bin/env ruby
#encoding: utf-8

module NONN

  require 'digest'

  class Main

    def self.noui m
      return true if Digest::SHA1.hexdigest(m.to_s)[-1].ord % 2 == 1
      return false
    end

  end
end

class TrueClass
  def ass
    return "Oui"
  end
end

class FalseClass
  def ass
    return "Non"
  end
end
