Gem::Specification.new do |s|
  s.name        = 'niouininon'
  s.version     = '1.0.1'
  s.date        = '2014-02-28'
  s.summary     = "Initial"
  s.description = "A simple Yes or No Gem"
  s.authors     = [
  		  "poulet_a"
		  ]
  s.email       = "poulet_a@epitech.eu",
  s.files       = [
  		  "lib/niouininon.rb",
  		  "lib/niouininon/niouininon.rb",
		  ]
  s.homepage    = "https://gitlab.com/Sopheny/niouininon"
  s.license     = "GNU/GPLv3"
end